# To install dependencies
use `npm install` or `yarn install` (Use yarn!)

# To run the app
After installing the dependencies, to run this app in 
terminal put, `yarn start`. Its okay, it'll run the app
in your default port.

# Check our repository dependecies
First check whether we used our `git repos` as a dependency.
If its there in 'package.json', check the branch we have used
(In default it'll be MAIN or MASTER).

# If there any issues
contact us : `balajivish2020@gmail.com` or `planethool20@gmail.com`
We'll give a demo for you how to set it up.
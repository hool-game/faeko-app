import cards from '@hool/cards'
import FaekoLogic from '@faeko/logic'

function Faeko(data, client) {

  
  // Inherit behaviour and functions from FaekoLogic
  FaekoLogic.apply(this, data)

  // Hack: the data property isn't being set so
  // we're setting it again
  this.data = data
  console.log("DATA FROM FAEKO.JS : ", this.data);

  if(this.data.take || this.data.flip){
    console.log("Take or flip above", this.data.take, "\n flip ", this.data.flip )
  }
  // A client for sending and receiving messages
  // from the server. This is assumed to handle
  // all the messages in XMPP/JSON/whatever format
  // is being used
  this.client = client

  // Handle client events
  this.client.on('start', ({ game, user }) => {
    console.log(`${user} is ready to play at ${game}`)

    // Update the user
    let userIndex = this.data.occupants.findIndex(o =>
      o.nickname == user)

    // If the user is there, add them in
    if (userIndex >= 0) {
      this.data.occupants[userIndex].ready = true
      return
    } else {
      console.warn(`Got start message from non-existent user ""${user}"!`)
    }
  })

  this.client.on('roomJoin', (presence) => {
    // Add user to occupant list if needed
    if (!this.data.occupants) {
      this.data.occupants = []
    }

    let userIndex = this.data.occupants.findIndex(o =>
      o.jid == presence.user.toString())

    // If user is already there, just update
    if (userIndex >= 0) {
      this.data.occupants[userIndex].role = presence.role
      return
    }

    // Otherwise, add
    this.data.occupants.push({
      jid: presence.user.toString(),
      role: presence.role,
      nickname: presence.nickname,
    })

  })

  this.client.on('roomDisconnect', (presence) => {
    // FIXME: only do this for exit, not disconnect!
    // Remove the user from the occupants list

    if (!this.data.occupants) {
      this.data.occupants = []
    }

    let userIndex = this.data.occupants.findIndex(o =>
      o.jid == presence.user.toString())

    // If the user is there, remove them
    if (userIndex >= 0) {
      let removedUser = this.data.occupants[userIndex]
      this.data.occupants[userIndex].inactive = true

      // change the inactive flag to true, if the user is a player
      this.removePlayer(removedUser.jid)
    }

  })

  // for removing players if anyone exited in the midgame
  this.removePlayer = (jid) => {

    // check for the players array
    if (!this.data?.players || !this.data.players.length) return

    let playerIndex = this.data.players.findIndex((p) => p.jid == jid)

    // TODO : Make different handler for take decisions for 
    // other players
    if (this.data.take && playerIndex === this.data.take.player) {
      this.data.take.enable = false
    }

    // if the disconnected user is not a player
    if (playerIndex == -1) return

    // changing the inactive flag to true
    this.data.players[playerIndex].inactive = true

    // if all the other players are inactive means game over
    let activePlayers = this.data.players.filter(p => !p.inactive)

    // if only one active player is left means game over 
    if (activePlayers.length === 1)
      this.data.gameOver = true

    return
  }

  this.client.on('roomExit', (presence) => {
    // Remove the user from the occupants list
    if (!this.data.occupants) {
      this.data.occupants = []
    }

    let userIndex = this.data.occupants.findIndex(o =>
      o.jid == presence.user.toString())

    // If the user is there, remove them
    if (userIndex >= 0) {
      let removedUser = this.data.occupants[userIndex]
      this.data.occupants[userIndex].inactive = true

      // change the inactive flag to true, if the user is a player
      this.removePlayer(removedUser.jid)
    }

  })

  // Handle state updates
  this.client.on('state', (state) => {
    // Okay, so there's a lot to unpack here; let's take this
    // slowly.

    // Start with the deck, since that's the most important
    if (state.deck) {

      if (!this.data.deck) {
        this.data.deck = new cards.Deck()
      }

      this.data.deck.cards = state.deck
    }

    // Now comes the major part: the players!
    if (state.players) {

      // Let's make sure there *are* players to start with...
      if (!this.data.players) {
        this.data.players = []

        // Automatically compute the nicknames (this can be
        // overridden later
        let playingOccupants = this.data.occupants.filter(o =>
          o.role.slice(0,6) == 'player')
          //     \________/
          //          \
          //  _________\_________
          // /                   \
          // Forward compatibility: if we decide to make player
          // roles something like 'player-0', 'player-1', etc.
          // later, this logic will still work.

          // A rudimentary check: if there are more players in
          // the list than we have track of occupants, it means
          // there's something wrong and we should probably just
          // wait for the backend to tell us what the names are.
          if (state.players.length <= playingOccupants.length) {
            playingOccupants.forEach(o => {
              this.data.players.push({
                nickname: o.nickname,
                jid: o.jid,
              })
            })
          }
      }

      // Now we loop over the data that's just come in
      state.players.forEach((player,i) => {

        // If the player is undefined, it means there's
        // no data, so we can just skip it
        if (!player) return

        // Now let's make sure we have a record for the
        // player, and see what's there to extract from it.
        if (!this.data.players[i]) {
          this.data.players[i] = {}
        }

        // Get the nickname
        if (player.nickname) {
          this.data.players[i].nickname = player.nickname
        }

        // Get the hand
        if (player.hand) {
          this.data.players[i].hand = player.hand
          // Sort the cards!
          if (!this.data.deck) this.data.deck = new cards.Deck()
          this.data.players[i].hand.sort(this.data.deck.compare)
        }

        // exposed means player left the room
        if (player.exposed) {
          this.data.players[i].exposed = player.exposed
        }

        // Get the bid
        if (typeof(player.bid) == 'number' && !isNaN(player.bid)) {
          this.data.players[i].bid = player.bid
        }

        // Get the score (this is the running score, so it
        // isn't essential to the game per se, but needs
        // must...)
        if (typeof(player.score) == 'number' && !isNaN(player.score)) {
          this.data.players[i].score = player.score
        }

        // That's it for the loop! This comment is just there
        // to remind you what the following close-brackets are
        // all about ----.----.
        //               |    |
      }) // <------------'    |
    } // <--------------------'

    // Time to process the tricks. We'll assume the entire set
    // of tricks is sent at once; there's no "partial tricks
    // update for that round" concept here!
    if (state.tricks) {

      console.log("INSIDE STATE ", state.tricks)

      // Again, we make sure there are tricks
      if (!this.data.tricks) {
        this.data.tricks = new cards.Tricks()
      }

      // set trump here if specified
      let trump = state.tricks.find((el)=>el.name==='trump').children[0]
      this.data.tricks.trumpSuit = trump

      // Now we loop through them, but this is a significantly
      // smaller loop than last time.
      // console.log("TRICK MAP ", this.data.tricks)

      window.state = state
      
      this.data.tricks.loadArray(state.tricks
        .map(trick => (
          trick.map(play => ({
            player: play.player,
            card: {
              suit: play.suit,
              rank: play.rank,
            },
          }))
        )))

      console.log("INSIDE STATE TRICKS ", this.data.tricks, this.data.tricks.length)
      // Process flip state
      if (this.data.flippedTricks) {
        this.data.flippedTricks.forEach((flipped, i) => {
          if (flipped && this.data.tricks[i]) {
            this.data.tricks[i].flipped = true
          }
        })
      }

      // Don't forget to flip the trick object itself!
      if (
        this.data.tricks.length &&
        this.data.tricks[this.data.tricks.length-1].flipped
      ) {
        this.data.tricks.flipped = true
      }
    }

    // And that's it! We're all unpacked :D
  })

  this.client.on('bid', (bid) => {

    // Figure out which player made the bid
    let player = bid.player

    // If it wasn't specified, deduce from nickname
    if (typeof(bid.player) != 'number') {
      bid.player = this.data.occupants
        .find(o => o.nickname == bid.nickname)
        .player
    }

    // Save it there
    this.data.players[player].bid = bid.value
  })

  this.client.on('card', (card) => {

    // Figure out which player played the card
    let player = card.player

    // If it wasn't specified, deduce from nickname
    if (typeof(card.player) != 'number') {
      card.player = this.data.occupants
        .find(o => o.nickname == card.nickname)
        .player
    }

    // Play the card
    this.play(Number(player), {
      suit: card.suit,
      rank: card.rank,
    }, false) // <----.
    //  .-------------'
    //  |
    // That "false" means we don't try to remove the
    // card from the hand if we don't have the hand
    // in the first place. (We don't have other players'
    // hands, so we shouldn't try to see if they have
    // a card, remove it, etc. If we *do* have the hand,
    // eg. our own hand, then it will get processed as
    // usual).

    // TODO : Make different handler for take decisions for 
    // other players

    // if(this.data.take || this.data.flip){
    //   console.log("Take or flip ", this.data.take, "\n flip ", this.data.flip )
    // }
    // if (this.data.take) { 
    //   this.data.take.enable = false
    // }

    // TODO: Add the next trick after a delay

  })

  // Not much to handle for flip: we assume the
  // backend has already handled it!
  this.client.on('flip', (flip) => {
    // TODO : Make different handler for take decisions for 
    // other players
    console.log("FLIP OBJECT ", flip, this.data.flip)
    let player = flip.player

    if (typeof (player) != 'number' && !isNaN(player)) {
      player = this.data.players
        .findIndex(o => o.nickname == flip.nickname)
    }

    let flipObj = {}
    flipObj.enable = true
    flipObj.player = player
    this.data.flip = flipObj

    console.log("FLIP OBJECT after some time \n ", flip, this.data.flip)

    // if (this.data.flip) {
    //   this.data.flip.enable = false
    // }

    this.data.tricks.flip()
  })

  // this is for take of flip 
  this.client.on('take', (take) => {
    // Figure out which player is having a chance to flip or take the trick
    let player = take.player  

    // If it wasn't specified, deduce from nickname
    if (typeof (player) != 'number' && !isNaN(player)) {
      player = this.data.players
        .findIndex(o => o.nickname == take.nickname)
    }

    this.data.takeMade = true
    console.log("this won't be happen", this.data.takeMade)
    
    let takeObj = {}
    takeObj.enable = true
    takeObj.player = player
    this.data.take = takeObj

    // if (this.data.take) {
    //   this.data.take.enable = false
    // }
    this.data.tricks.addTrick()

    console.log("TAKE OBJ -- fakeo-client", takeObj)
  })  


  //chat handler for incomming chat
  this.client.on('chat', (chat) => {
    if (!this.data.messages) {
      this.data.messages = []
    }

    this.data.messages.push({
      from: chat.nickname,
      text: chat.text
    })
  })

  this.client.on('takeOccured', (takeScene) => {
    console.log("SCENE POTTA IPDI THAN", takeScene)
  })

}

export default Faeko
